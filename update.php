<?php
require_once('inc/config.php');

if($_REQUEST['id']!=''){

  $stmt = $db->prepare("UPDATE task1 SET description=?, due_at=?, priority=?,assigned_to=? WHERE id = ?");
  $stmt->execute(array($_REQUEST['description'],$_REQUEST['due'],$_REQUEST['priority'],$_REQUEST['assigned_to'],$_REQUEST['id']));
}else{
  $current_date = date('Y-m-d');
  $stmt = $db->prepare("INSERT INTO task1(description,created_at,due_at,priority,assigned_to,created_by) VALUES(?,?,?,?,?,?)");
  $stmt->execute(array($_REQUEST['description'],$current_date,$_REQUEST['due'],$_REQUEST['priority'],$_REQUEST['assigned_to'],$_SESSION['userid']));
}

header('Location: index.php');
 ?>
