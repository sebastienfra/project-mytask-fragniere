-- Adminer 4.2.5 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `task1`;
CREATE TABLE `task1` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `description` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `due_at` datetime NOT NULL,
  `assigned_to` int(11) NOT NULL,
  `status` enum('open','close') NOT NULL,
  `priority` enum('1','2','3','4') NOT NULL,
  `done_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `task1` (`id`, `description`, `created_at`, `created_by`, `due_at`, `assigned_to`, `status`, `priority`, `done_by`) VALUES
(45,	'task 1',	'2017-06-25 00:00:00',	4,	'2017-06-26 00:00:00',	1,	'open',	'1',	NULL),
(53,	'brzb',	'2017-06-25 00:00:00',	1,	'2017-06-26 00:00:00',	1,	'close',	'3',	NULL),
(55,	'sdcs',	'2017-06-25 00:00:00',	1,	'2017-06-26 00:00:00',	1,	'open',	'1',	NULL),
(62,	'as',	'2017-06-25 00:00:00',	1,	'2017-06-26 00:00:00',	1,	'open',	'1',	NULL),
(63,	'as',	'2017-06-25 00:00:00',	1,	'2017-06-26 00:00:00',	1,	'open',	'1',	NULL),
(64,	'as',	'2017-06-25 00:00:00',	1,	'2017-06-26 00:00:00',	1,	'close',	'1',	NULL),
(65,	'ydcydv',	'2017-06-26 00:00:00',	1,	'2017-06-27 00:00:00',	1,	'open',	'1',	NULL),
(66,	'sdsd',	'2017-06-26 00:00:00',	1,	'2017-06-27 00:00:00',	1,	'open',	'1',	NULL);

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `email` varchar(128) NOT NULL,
  `password` varchar(128) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `user` (`id`, `name`, `email`, `password`) VALUES
(1,	'sebastien',	'seb.fragniere',	'sebastien'),
(14,	'ccc',	'seb.fragniere',	'sebastien'),
(15,	'hbjh',	'seb.fragniere',	'sebastien'),
(16,	'hbjh',	'seb.fragniere',	'sebastien');

-- 2017-06-26 11:45:27