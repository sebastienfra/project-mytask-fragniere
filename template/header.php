<header class="header">
  <div class="top-bar">
    <div class="top-bar-left">
      <a href="#" class="top-bar-left-btn" data-toggle="offCanvasLeft"><i class="fa fa-bars fa-2x" aria-hidden="true"></i></a>
      <a href="index.php">
        <i class="fa fa-check-square-o fa-2x" aria-hidden="true"></i>
        <title class="top-bar-left-title">Task List</title>
      </a>
    </div>
      <div class="top-bar-right <?php echo isset($id)?'':'hide';?>">

        <ul class="dropdown menu header-usermenu" data-dropdown-menu>
          <li>

          </li>
          <li><a class="header-usermenu-link" href="#">
            <span  class="hide-for-small-only">
              <?php if(isset($user)){echo $user;}; ?>
            </span>
            <img class="header-usermenu-img" src="assets/img/<?php echo $id;?>.png"/></a>
            <ul class="menu header-usermenu-menu">
              <li><a href="logout.php">Logout</a></li>
              <li><a href="newuser.php?id=<?php echo $id; ?>">Edit user</a></li>
            </ul>
          </li>
        </ul>
      </div>
    </div>
  </div>
</header>
