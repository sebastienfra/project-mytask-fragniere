<!doctype html>

<?php
require_once('inc/config.php');

$modify = FALSE;

$current_date = date('Y-m-d', strtotime('+1 day'));


if(isset($_REQUEST['id'])){
  $id = $_REQUEST['id'];
  $modify = TRUE;
  $stmt = $db->prepare($q_edit);
  $stmt->execute(array($id));
  $tasks = $stmt->fetchAll();


  $due = $tasks[0]['due_at'];
  $description = $tasks[0]['description'];
  $priority = $tasks[0]['priority'];
  $assigned_to = $tasks[0]['assigned_to'];
}
?>

<html class="no-js" lang="en">
    <?php require_once('template/head.php'); ?>
    <body>
      <?php require_once('template/header.php'); ?>

<div class="off-canvas-wrapper">
  <div class="off-canvas position-left" id="offCanvasLeft" data-transition="push" data-off-canvas>
    <?php require_once('template/offcanvas.php'); ?>
  </div>

  <main class="off-canvas-content main" data-off-canvas>



      <form class="form-edit" method="post" action="update.php">
          <ul>
              <input id="id" name="id" type="hidden" value="<?php echo $modify?$id:''; ?>"/>
            <li class="row medium-6 large-4 columns">
              <label for="due">Due date</label>
              <input class="form-edit-input" name="due" id="due" type="date" value="<?php echo $modify?$due:$current_date; ?>"/>
            </li>
            <li class="row medium-6 large-4 columns">
              <label for="priority">Priority</label>
              <input class="form-edit-input" name="priority"  id="priority" value="<?php echo $modify?$priority:'1'; ?>" min="1" max="4" place type="number"/>
            </li>
            <li class="row medium-6 large-4 columns">
              <label for="description">Description</label>
              <textarea class="form-edit-input" name="description"  id="description" autofocus required><?php echo $modify?$description:''; ?></textarea>
            </li>
            <li class="row medium-6 large-4 columns">
              <label for="description">Assigned to</label>
              <select class="form-edit-input" name="assigned_to"  id="assigned"">
              <?php
              $query = $db -> query('SELECT * FROM user');
              while($data = $query -> fetch()):
              ?>
                <option value="<?php echo $data['id']; ?>"><?php echo $data['name']; ?></option>
              <?php
              endwhile;
              ?>
            </select>
            </li>
            <li class="row medium-6 large-4 columns">
              <input class="form-edit-input submit"  type="submit"  value="Submit">
            </li>
          </ul>
        </form>


      </main>
      </div>
      <?php require_once('template/footer.php'); ?>
    </body>
</html>
