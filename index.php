<!doctype html>

<?php require_once('inc/config.php'); ?>

<html class="no-js" lang="en">
  <?php require_once('template/head.php'); ?>
  <body>
    <?php require_once('template/header.php'); ?>


<div class="off-canvas-wrapper">
  <div class="off-canvas position-left" id="offCanvasLeft" data-transition="push" data-off-canvas>
    <?php require_once('template/offcanvas.php'); ?>
  </div>
  <main class="off-canvas-content main" data-off-canvas>
    <?php
      require_once('tasklist.php');
    ?>
  </main>
</div>
    <?php require_once('template/footer.php'); ?>
  </body>
</html>
