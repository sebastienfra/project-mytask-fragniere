
    <h1 class="titre">Liste des taches</h1>
  <ul class="list">
      <li class="row hide-for-small-only list-description">

        <a href="index.php?tri=id"><span class=" hide-for-medium-only hide-for-small-only list-task-id">ID<i class="fa fa-caret-down" aria-hidden="true"></i></span></a>

        <span class=" hide-for-medium-only  hide-for-small-only list-task-description2">Description</span>

        <a href="index.php?tri=date"><span class=" hide-for-medium-only hide-for-small-only list-task-date2">Date<i class="fa fa-caret-down" aria-hidden="true"></i></span></a>

        <a href="index.php?tri=due"><span class=" hide-for-medium-only  hide-for-small-only list-task-due2">Due</span></a>

        <a href="index.php?tri=prio"><span class=" hide-for-medium-only hide-for-small-only list-task-priority">Priorite<i class="fa fa-caret-down" aria-hidden="true"></i></span></a>

        <span class=" hide-for-medium-only hide-for-small-only list-task-status">Status<i class="fa fa-caret-down" aria-hidden="true"></i></span>

        <span class=" hide-for-medium-only hide-for-small-only list-task-delete">Delete</span>
    </li>

        <?php
                                            $query = $db -> prepare('SELECT
                                            task1.id,
                                            description,
                                            created_at,
                                            due_at,
                                            priority,
                                            status,
                                            creator.id as creator_id,
                                            creator.name as creator_name,
                                            assignee.id as assignee_id,
                                            assignee.name as assignee_name
                                            FROM task1
                                            INNER JOIN user as creator on created_by = creator.id
                                            LEFT JOIN user as finishor on done_by = finishor.id
                                            INNER JOIN user as assignee on assigned_to = assignee.id');
                                            $query -> execute();
    if(isset($_REQUEST['tri'])){
  if($_REQUEST['tri']=="id"){
              $query = $db -> prepare('SELECT
                                            task1.id,
                                            description,
                                            created_at,
                                            due_at,
                                            priority,
                                            status,
                                            creator.id as creator_id,
                                            creator.name as creator_name,
                                            assignee.id as assignee_id,
                                            assignee.name as assignee_name
                                            FROM task1
                                            INNER JOIN user as creator on created_by = creator.id
                                            LEFT JOIN user as finishor on done_by = finishor.id
                                            INNER JOIN user as assignee on assigned_to = assignee.id
                                            order by task1.id ');
                                            $query -> execute();
                                            }
                      if($_REQUEST['tri']=="prio"){
                        $query = $db -> prepare('SELECT
                                            task1.id,
                                            description,
                                            created_at,
                                            due_at,
                                            priority,
                                            status,
                                            creator.id as creator_id,
                                            creator.name as creator_name,
                                            assignee.id as assignee_id,
                                            assignee.name as assignee_name
                                            FROM task1
                                            INNER JOIN user as creator on created_by = creator.id
                                            LEFT JOIN user as finishor on done_by = finishor.id
                                            INNER JOIN user as assignee on assigned_to = assignee.id
                                            order by task1.priority');
                                            $query -> execute();
                                            }

                      if($_REQUEST['tri']=="date"){
                                            $query = $db -> prepare('SELECT
                                            task1.id,
                                            description,
                                            created_at,
                                            due_at,
                                            priority,
                                            status,
                                            creator.id as creator_id,
                                            creator.name as creator_name,
                                            assignee.id as assignee_id,
                                            assignee.name as assignee_name
                                            FROM task1
                                            INNER JOIN user as creator on created_by = creator.id
                                            LEFT JOIN user as finishor on done_by = finishor.id
                                            INNER JOIN user as assignee on assigned_to = assignee.id
                                            order by task1.due_at');
                                            $query -> execute();
                                            }
                      if($_REQUEST['tri']=="due"){
                                            $query = $db -> prepare('SELECT
                                            task1.id,
                                            description,
                                            created_at,
                                            due_at,
                                            priority,
                                            status,
                                            creator.id as creator_id,
                                            creator.name as creator_name,
                                            assignee.id as assignee_id,
                                            assignee.name as assignee_name
                                            FROM task1
                                            INNER JOIN user as creator on created_by = creator.id
                                            LEFT JOIN user as finishor on done_by = finishor.id
                                            INNER JOIN user as assignee on assigned_to = assignee.id
                                            order by task1.create_at ');
                                            $query -> execute();
                                        
                        }
            }

            while($row = $query -> fetch()):

        ?>
          <?php $done = $row['status'] == 'close'; ?>
        <li class="row list-task <?php echo ($done)?'done':''; ?>">
            <a class="linkedit" href="edit.php?id=<?php echo $row['id']; ?>">
                <div>
                <span class=" hide-for-small-only list-task-id"><?php echo $row['id']; ?> :</span>
                <span class=" list-task-description"><?php echo $row['description']; ?></br><span class=" list-task-assigne-to">Pour <?php echo $row['assignee_name']; ?> de <?php echo $row['creator_name']; ?></span></span>
                <span class=" hide-for-medium-only hide-for-small-only list-task-date"><?php echo getRelativeTime($row['created_at']); ?></span>
                <span class=" list-task-due"><?php echo getRelativeTime($row['due_at']); ?></span>
                <span class=" hide-for-small-only list-task-priority"><?php echo $row['priority']; ?></span>
                </div>
            </a>
            <span class="list-task-status">
                <a href="#" data-done="<?php echo $row['id'] ?>" class="fa fa-check-square-o" ></a>
            </span>
            <span class="list-task-delete">
                <a href="#" data-delete="<?php echo $row['id'] ?>" class="fa fa-trash-o" ></a>
            </span>
        </li>
    <?php endwhile; ?>
</ul>

<a id="btn-add" href="edit.php"><i class="fa fa-plus fa-2x"></i></a>
