$(document).foundation();



$('[data-delete]').click(function(e){
	var elt = $(this);
	$.ajax({
		url: 'delete.php?id='+$(elt).data('delete')
	})
	.success(function(){
		$(elt).parents('.list-task').fadeOut();
	})
});

$('[data-done]').click(function(e){
	element = $(this);
	line = element.parents();
	taskid = element.data('done');
	
	$.ajax({
		url: 'done.php?id='+ taskid
	})
	.done(function( data ){
		line.toggleClass('done');
	})
});

$('[data-deleteuser]').click(function(e){
  var id = $(this).data('deleteuser');
  var line = $(this);
  $.ajax({
    url: "deleteUser.php?id=" + id,
  }).done(function(data) {
    $(id).parents('.list-user').fadeOut();
  });

});