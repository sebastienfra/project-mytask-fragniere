<!doctype html>

<?php
require_once('inc/config.php');

$modify = FALSE;

if(isset($_REQUEST['id'])){
  $id = $_REQUEST['id'];
  $modify = TRUE;
  $stmt = $db->prepare($q_edit_user);
  $stmt->execute(array($id));
  $tasks = $stmt->fetchAll();


  $name = $tasks[0]['name'];
  $email = $tasks[0]['email'];
  $password = $tasks[0]['password'];
}
?>

<html class="no-js" lang="en">
    <?php require_once('template/head.php'); ?>
    <body>
      <?php require_once('template/header.php'); ?>

<div class="off-canvas-wrapper">
  <div class="off-canvas position-left" id="offCanvasLeft" data-transition="push" data-off-canvas>
    <?php require_once('template/offcanvas.php'); ?>
  </div>

  <main class="off-canvas-content main" data-off-canvas>

        <form class="form-edit" method="post" action="userupdate.php">
          <ul class="new-user-label">
          <input id="id" name="id" type="hidden" value="<?php echo $modify?$id:''; ?>"/>
            <li class="row medium-6 large-4 columns">
              <label class="user-label-name" for="name">Name</label>
              <input class="form-edit-input" name="name"  id="name" type="text" value="<?php echo $modify?$name:''; ?>" autofocus required/>
            </li>            
            <li class="row medium-6 large-4 columns">
              <label class="user-label-email" for="email">Email</label>
              <input class="form-edit-input" name="email"  id="email" type="text" value="<?php echo $modify?$email:''; ?>" autofocus required/>
            </li>            
            <li class="row medium-6 large-4 columns">
              <label class="user-label-password" for="password">Password</label>
              <input class="form-edit-input" name="password"  id="password" type="password" value="<?php echo $modify?$password:''; ?>" autofocus required/>
            </li>
            <li class="row medium-6 large-4 columns">
              <input class="form-edit-input submit"  type="submit"  value="Submit">
            </li>
          </ul>
        </form>


  </main>
</div>
<?php require_once('template/footer.php'); ?>
</body>
</html>
