<?php
require_once('inc/config.php');
require_once('inc/security.php');
?>
<!doctype html>
<html class="no-js" lang="fr">
<html class="no-js" lang="en">
<?php require_once('template/head.php'); ?>

<body>
    <?php require_once('template/header.php'); ?>
   <div class="off-canvas-wrapper">
   <div class="off-canvas position-left" id="offCanvasLeft" data-transition="push" data-off-canvas>
      <?php require_once('template/offcanvas.php'); ?>
   </div>

   <main class="off-canvas-content main" data-off-canvas>

      <div id="contener">
   		<p id="titre">
   		   	<h1>Manuel d'utilisateur</h1>
   		</p>
         <p>
             En arrivant sur MyTask, il vous faudra vous connecter avec votre compte utilisateur en remplissant les champs email et mot de passe ci-dessous :<br />
             <img src="img/tuto1.png" alt="login" title="login page" />
         </p>
         <p>
             Une fois connectez-vous arriverez sur la page principale ou son lister toutes vos taches. Il est possible de les trier par ID, date, priorité ou statut en cliquant simplement sur le label désiré :<br />
             Il est possible de modifier une taches en cliquant dessu, de la cocher comme faite ou de la supprimer.
             <img src="img/tuto2.png" alt="accueil" title="page d'accueil" />
         </p>
         <p>
             Le menu est acsésible en cliquant sur les trois bars latéraux en haut à gauche :<br />
             <img src="img/tuto3.png" alt="menu" title="menu principale" />
         </p>
         <p>
             Il y a deux méthode pour créer une nouvelle tache :<br />
             - Cliquer sur le bouton vert avec un +.<br />
             - Dans le menu latéral en choisissant ajouter une nouvelle tache.
             <img src="img/tuto4.png" alt="update" title="formulaire bouvelle tache" />
         </p>
         <p>
             Pour se déconnecter il suffit de passer votre curseur sur votre photo de profil et de cliquer sur logout. IL vous est aussi possible de modifier votre profil :<br />
             <img src="img/tuto7.png" alt="profil" title="profil logout" />
         </p>
         <p>
             La liste des utilisateurs fonctionne comme celle des taches, il suffit de cliquer sur un profil pour le modifier, sur la poubelle pour le supprimer ou sur le bouton plus pour en créer un nouveau :<br />
             <img src="img/tuto5.png" alt="user" title="user list" />
         </p>
         <p>
             Voici le formulaire de création et de modification d'un utilisateur :<br />
             <img src="img/tuto6.png" alt="newuser" title="new user" />
         </p>
         <h4>Bonne utilisation ;)</h4>


      </div>
   </main>
   <?php require_once('template/footer.php');   ?>
</body>