<?php
require_once('inc/config.php');
require_once('inc/security.php');
?>
<!doctype html>
<html class="no-js" lang="fr">
<html class="no-js" lang="en">
<?php require_once('template/head.php'); ?>

<body>
    <?php require_once('template/header.php'); ?>
   <div class="off-canvas-wrapper">
   <div class="off-canvas position-left" id="offCanvasLeft" data-transition="push" data-off-canvas>
      <?php require_once('template/offcanvas.php'); ?>
   </div>

   <main class="off-canvas-content main" data-off-canvas>

      <div id="contener">
   		<p id="titre">
   		   	<h1>About</h1>
   		</p>
             <p>Ce projet a pour but de réaliser un gestionnaire de taches. Nous avons tout au long de l'année appris les différentes technologies du web, tel que HTML/CSS, PHP, AJAX, MYSQL. Pour la réalisation de ce projet nous avons du combiner tous ces languages afin de créer une application permettant d'ajouter une nouvelle tache, de la supprimer, la modifier, ou d'ajouter un utilisateur, le supprimer.</p>
             <p> Toutes les données (des tâches et utilisateurs) se trouvent sur une base de donner MYSQL. Le projet devais être en responcive design et regulièrement sauver sur un Bitbucket.</p>
             <p>Ce travail nous aura permis de découvir la programmation web et de mieux comprendre son fonctionnement.</p>
             </div>
      </div>
   </main>
   <?php require_once('template/footer.php');   ?>
</body>