<?php
require_once('inc/config.php');
require_once('inc/security.php');
?>
<!doctype html>
<html class="no-js" lang="fr">
<html class="no-js" lang="en">
<?php require_once('template/head.php'); ?>

<body>
    <?php require_once('template/header.php'); ?>
   <div class="off-canvas-wrapper">
   <div class="off-canvas position-left" id="offCanvasLeft" data-transition="push" data-off-canvas>
      <?php require_once('template/offcanvas.php'); ?>
   </div>

   <main class="off-canvas-content main" data-off-canvas>

      <div id="contener">
   		<p id="titre">
   		   	<h1>Présentation Personnelle</h1></br>
   		</p>
         <div class="photo" >
            <img src="assets/img/1.png" alt ="Sébastien Fragnière" title="ma photo"/ height="200" width="200">
         </div>
         <p id="adresse">
            Sébastien Fragnière 
            <br>Chemin de la Buchille 68
            <br>1630 Bulle<br>
         </p>
		<p id="infoperso">
			Téléphone mobile    079 / 962 82 97  
			<br>Téléphone domicile  026 / 913 94 40
			<br>Messagerie:<a href="mailto:seb.fragniere@gmail.com">seb.fragniere@gmail.com</a>
		</p>
      <div class="clear"> </div>
	<h2>Domaine de compétences:</h2><br/>
      <ul>
         <li class="soustitre">Objectif</li><br>
         <ul>
         <li>Après avoir travaillé dans le domaine des télécommunications chez Swisscom, je suis aujourd'hui à la Haute Ecole d’Ingénieurs de Fribourg. J’ai fait un apprentissage d’électronicien chez <a href=“http://www.vibro-meter.com“ title="Meggitt website" target="_blank">Meggitt SA</a> à Villars-sur-Glâne, complété par une maturité technique à l’EMF. J’ai travaillé durant mon apprentissage dans divers départements de mon entreprise. Je suis passionné par les nouvelles technologies et j'aime me fixer des défis passionnants.
		 </li><br>
      </ul>
     </ul>
<!--..................................................-->
      <ul>
      	<li class="soustitre">Scolarité / Formation</li><br>
      	<ul>
            <li>2015-2016  Maturité fédérale technique</li>
            <li>2008-2012  Apprentissage chez Meggitt SA à Villars-sur-Glâne</li>
            <li>2005-2008  CO de Bulle</li>
            <li>1999-2005  Ecole primaire à Bulle</li>
      	</ul>
      </ul>	
<!--..................................................-->
      <ul>
      	<li class="soustitre">Expérience professionnelle</li><br>
      	<ul>
            <li>De septembre 2012 à mai 2013 : collaborateur technique temporaire chez Meggitt SA</li>
            <li>De juin à décembre 2013 : école de langues à Los Angeles et à Miami USA</li>
            <li>De mars à juillet 2014 : école de recrue, compagnie de chars à Thoune</li>
            <li>De novembre 2014 à juin 2015 : technicien de service chez Swisscom SA</li>
      	</ul>
      </ul>	

<!--..................................................-->
      <ul>
      	<li class="soustitre">Stages durant l’apprentissage</li><br>
      	<ul>
      		<li>Plusieurs stages en polymécanique durant l’apprentissage, bonnes bases sur des machines telles que fraiseuses, perceuses et tours.</li>
      		<li>Stage de layout sur l’Altium Designer.</li>
      		<li>Stage de dessin technique sur SolidWorks</li>
      	</ul>
      </ul>
<!--..................................................-->
     <ul>
      	<li class="soustitre">Armée</li><br>
      	<ul>
      		<li>Ecole de recrue dans les blindés à Thoune, comme conducteur de char, du 10 mars au 31 juillet 2014</li>
      		<li>3 CR effectués dans la compagnie de chars à Bure JU</li>
      	</ul>
      </ul>
<!--..................................................-->
      <ul>
      	<li class="soustitre">Langues</li><br>
      	<ul>
      		<li>Français	Langue maternelle</li>
      		<li>Anglais		Niveau FCE</li>
      		<li>Allemand	Niveau scolaire</li>
      	</ul>
      </ul>
<!--..................................................-->
      <ul>
      	<li class="soustitre">Connaissances techniques</li><br>
      	<ul>
      		<li>Informatique: Internet, réseaux sociaux, Microsoft Office, Photoshop, etc. et de bonnes connaissances sur PC et Mac</li>
      		<li>Stage de layout sur l’Altium Designer.</li>
      		<li>Stage de dessin technique sur SolidWorks</li>
      	</ul>
      </ul>
      <div/>
      <?php require_once('template/footer.php');   ?>
   </body>

