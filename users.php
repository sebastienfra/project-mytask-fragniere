<?php
	require_once('inc/config.php');
	require_once('inc/security.php');
?>
<!doctype html>
<html class="no-js" lang="fr">
<html class="no-js" lang="en">
<?php require_once('template/head.php'); ?>

<body class="body-user">
    <?php require_once('template/header.php'); ?>
	<div class="off-canvas-wrapper">
  	<div class="off-canvas position-left" id="offCanvasLeft" data-transition="push" data-off-canvas>
    	<?php require_once('template/offcanvas.php'); ?>
  	</div>

  	<main class="off-canvas-content main" data-off-canvas>
		<div class="row">
			<h1 class="page-user-title">Liste des utilisateurs</h1>
			<ul class="list2">
				<li>
					<span class=" hide-for-small-only list-user-id">ID</span>
					<span class=" list-user-user">Utilisateur</span>
					<span class=" hide-for-small-only list-user-email">Email</span>
					<span class=" list-user-action">Actions</span>
				</li>
				<?php
		          $query = $db -> query('SELECT * FROM user');
		          while($data = $query -> fetch()):
		        ?>
				<li class="list-user">
					<a class="linkedit" href="newuser.php?id=<?php echo $data['id']; ?>">
				        <span class=" hide-for-small-only list-user-id">
				            <?php echo $data['id']; ?>
				        </span>
				        <span class=" list-user-user">
				            <?php echo $data['name']; ?>
				        </span>
						<span class=" hide-for-small-only list-user-email">
				            <?php echo $data['email']; ?>
				        </span>
				    </a>
	            	<span class="list-task-delete">
	                	<a href="#" data-deleteuser="<?php echo $data['id'] ?>" class="fa fa-trash-o" ></a>
	            	</span>
		        </li>
	        <?php endwhile; ?>
			</ul>
		</div>
		<a id="btn-add" href="newuser.php"><i class="fa fa-plus fa-2x"></i></a>
	</main>
	<?php require_once('template/footer.php');	?>
	</div>
  </body>
</html>
