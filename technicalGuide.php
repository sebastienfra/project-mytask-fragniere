<?php
require_once('inc/config.php');
require_once('inc/security.php');
?>
<!doctype html>
<html class="no-js" lang="fr">
<html class="no-js" lang="en">
<?php require_once('template/head.php'); ?>

<body>
    <?php require_once('template/header.php'); ?>
   <div class="off-canvas-wrapper">
   <div class="off-canvas position-left" id="offCanvasLeft" data-transition="push" data-off-canvas>
      <?php require_once('template/offcanvas.php'); ?>
   </div>

   <main class="off-canvas-content main" data-off-canvas>

      <div id="contener">
   		<p id="titre">
   		   	<h1>Manuel technique</h1>
          <h3>Table des matières</h3>
          <ul class="list-manuel">
            <li><a href="#introduction">1 Introduction</a></li>
            <li><a href="#fonctionnement">2 Fonctionnement des pages</a></li>
            <li><a href="#maquettes">3 Maquettes</a></li>
          </ul>
   	<h3 id="introduction">1 Introduction</h3>

    <p>MyTask est une application baser sur foundation qui emploie plusieur technologie actuel tel que HTML5 et CSS3, mais aussi PHP, MYSQL, AJAX et JavaScript avec le plugin JQuery. Il utilise aussi les logos de font awesome.</p>

    <h3 id="fonctionnement">2 Fonctionnement des pages</h3>


    <h4>2.1 Index.php</h4>
    <p>Index, php est la page principale de l'application. C'est là que l'utilisateur sera redirigé quand il sera logé correctement. Index ne fera qu'appeler : 

    </br>- config .php : qui permet de se connecter à la base de donner grâce à une requête PDO, mais aussi d'appeler le fichier de mise en forme des dates et du module de sécuriser qui vérifiait qu'il y a bien un id de session en cours.

    </br>- head .php : qui permet de définir les paramètres de base d'une page HTML, donc l'encodage, le titre dans l'onglet de la page et l'appelle des fichiers CSS..

    </br>- header.php : qui permet d'afficher la barre de menus en haut.

    </br>- tasklist.php : qui contient la logique pour afficher les taches.

    </br>- footer.php : qui affiche la barre du bas.

    fonctionne comme structure principale pour certaines pages. C’est-à-direque le contenu du «main»est changé dynamiquement en fonction des paramètres de l’URL. Cependant toute l'application ne marche pas comme ça comme le formulaire d’ajout/édition sont des fichiers Php qui incluent l'header et footer.</p>

    <h4>2.2 taslist.php</h4>
    <p>Se chargent d'afficher de façon dynamique les taches se trouvant dans la base de données. Il y a possibilité de modifier le tri des taches en cliquant sur l'argument à trier (par exemple par ID) seci au pour effet de recharger la page en passant par l'URL un argument, qui aura pour conséquence de modifier la requête SQL.</p>


    <h4>2.3 Edit.php</h4>
    <p>Le formulaire edit.php est utilisé pour l’ajout et la modification d'une tache. Il va en fonction qu'il y ait ou pas un ID passer en paramètre, préremplir les champs et modifier la tache courante ou créer une nouvelle tache dans la base de donner. Une fois les champs remplis et que l'utilisateur presse sur le bouton submit, le programme ira sur la update.php.</p>

    <h4>2.4 Update.php</h4>
    <p>Va se charger de faire les requêtes SQL. update va récupérer les données passées par edit.php et en fonction qu'un id de tache est fourni ou pas, il va soit créée une nouvelle tache ou en modifier une dans la base de données.</p>

    <h4>2.5 Login</h4>
    <p>Login.php va commander à l'utilisateur une adresse email et un mot de passe. Il va ensuite regarder dans la base de donner si une adresse mail et un mot de pass correspondent. Si oui il va donner un id de session à security.php qui permettra d'utiliser l'app tous le long que cet id est valide. Security.php se trouve dans toutes les pages du site sauf login.php ce qui fait que si l'on se déconnecte l'on est redirigé vers la page de login.</p>


    <h3 id="maquettes">3 Maquettes</h3>

    


      </div>
   </main>
   <?php require_once('template/footer.php');   ?>
</body>